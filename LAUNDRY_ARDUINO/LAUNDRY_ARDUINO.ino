#include "Arduino.h"
#include "Wire.h" 
#include <Keypad.h>
#include "RTClib.h"
#include "Adafruit_Thermal.h"
#include <LiquidCrystal_I2C.h>
#include "uEEPROMLib.h"
#include <ArduinoJson.h>
#include <SoftwareSerial.h>
#include "HX711.h"

#define debug
#define Proses           4 
#define EEPROM_CEK       10
#define max_var          12

//menu setting
#define input_pelayanan     0
#define input_jenis_pakaian 1
#define input_estimasi_Hari 2

//menu timbang
#define menu_timbang   0
#define menu_jenis     1
#define menu_pelayanan 2
#define menu_preview   3
#define Printing       4

#define periodaKeypad         100

String BuffMenu_input_layanan[5]={"0","Cuci", "Gosok", "Cuci&Gosok", "CuciGosokAntar"};
String BuffMenu_input_jenis  [6]={"0","Jas", "Baju", "Jeans", "Selimut", "Boneka"};
float hasil [Proses] = {0, 0, 0, 0};
int pilihan [Proses] = {0, 0, 0, 0};
float hitung = 0;
unsigned int harga_layanan[max_var]= {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned int harga_jenis [max_var]= {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned int harga[max_var]  = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned int buffEE[max_var] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int buffinput   = 0;
int pos_menu = 0;
unsigned long lastTime = 0;
unsigned long previousMillis_ap = 0; 
unsigned long timerDelay = 1000;
char tombolbuff ; 
int menu_proses_timbang = 0;
int loopKEYPADint = 0;
char loopKEYPADchar = 'x';
int min_GRAM = -20;
int printer_standby = 0;
float minimal = 0;
int  menu_input = 0;
int ID;

int dayR;
int monthR;
int yearR;

int Fday;
int Fmonth;
int Fyear;

int epoch ;
int plus = 0;

byte mychar[8] = {
  0b10001000,
  0b10001100,
  0b10001110,
  0b10001111,
  0b10001110,
  0b10001100,
  0b10001000,
  0b10000000
};
byte verticalLine[8] = {
  B00100,
  B00100,
  B00100,
  B00100,
  B00100,
  B00100,
  B00100,
  B00100
};  

byte char2[8] = {
  B00000,
  B00000,
  B00000,
  B11100,
  B00100,
  B00100,
  B00100,
  B00100
};

byte char1[8] = {
  0b00000,
  0b00000,
  0b00000,
  0b00111,
  0b00100,
  0b00100,
  0b00100,
  0b00100
};

byte char3[8] = {
  0b00100,
  0b00100,
  0b00100,
  0b00111,
  0b00000,
  0b00000,
  0b00000,
  0b00000
};

byte char4[8] = {
  0b00100,
  0b00100,
  0b00100,
  0b11100,
  0b00000,
  0b00000,
  0b00000,
  0b00000
};
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//LCD
LiquidCrystal_I2C lcd(0x27, 20, 4);

//tombol 4x4
const byte ROWS = 4; 
const byte COLS = 4; 

char keys[ROWS][COLS] = { 
{'1','2','3','A'}, 
{'4','5','6','B'}, 
{'7','8','9','C'}, 
{'*','0','#','D'}  
};

char pad[11][1] = {
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
};

byte rowPins[ROWS] = {46, 44, 42, 40}; 
byte colPins[COLS] = {38, 36, 34, 32}; 
Keypad keypad=Keypad(makeKeymap(keys),rowPins,colPins,ROWS,COLS);

byte padCounter;
char padChar;
bool padDitekan;
byte charCounter;
byte keySebelumnya;
char bufferKeypad[6];
char *bufferKeypadPtr;
long millisKeypad;

//RTC 
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Juma'at", "Sabtu"};
 DateTime now;
 DateTime future;

//thermal printer
Adafruit_Thermal printer(&Serial3);

//EEPROM untuk penyimpanan
uEEPROMLib eeprom(0x57);
unsigned int pos;

//komunikasi json esp
SoftwareSerial linkSerial(10, 11); 

//loadcell
#define DOUT  A3
#define CLK  A2
HX711 scale(DOUT, CLK);
float calibration_factor = 210;
int GRAM;
float KG;

//buzzer
#define buzzer 12

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

void beep()
{
  digitalWrite(buzzer, HIGH); 
  delay(100);  
  digitalWrite(buzzer, LOW); 
  delay(100);  
  digitalWrite(buzzer, HIGH); 
  delay(100); 
  digitalWrite(buzzer, LOW); 
  delay(100);   
}

void initRTC()
{  
//  if (! rtc.begin()) {
//    Serial.println("Couldn't find RTC");
//    Serial.flush();
//    abort();
//    }    
      rtc.begin();
  if (rtc.lostPower()) 
     {
    Serial.println("RTC lost power, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
     }
   //  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
 if (Serial) 
 {
  Serial.println("SerialConnect");
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
 }
}

uint32_t memWriteInt(uint8_t addr, uint32_t int_value)
{
  unsigned int ret = 0;
  if (!eeprom.eeprom_write(addr, int_value)) {
    ret = 1;
    Serial.println("Failed to store INT");
  } else {
    ret = 0;
    Serial.println("INT correctly stored");
  }
  delay(40);
  return ret;
}

uint32_t memReadInt(uint8_t addr)
{
  unsigned int val_temp = 0;
  eeprom.eeprom_read(addr, &val_temp);
  Serial.print("addr ");
  Serial.print(addr);
  Serial.print(" : ");
  Serial.println(val_temp);
  return val_temp;
}
void resetInput()
{
  bufferKeypadPtr = bufferKeypad;
  charCounter = 0;
  padCounter = 0;
  keySebelumnya = 0;
  padDitekan = false;
  lcd.blink();
}
void resetInput_init()
{
  bufferKeypadPtr = bufferKeypad;
  charCounter = 0;
  padCounter = 0;
  keySebelumnya = 0;
  padDitekan = false;
 
}
void load_nilai()
{
  harga_layanan[1]= buffEE[1];
  harga_layanan[2]= buffEE[2];
  harga_layanan[3]= buffEE[3];
  harga_layanan[4]= buffEE[4];

  harga_jenis [1] = buffEE[5]; 
  harga_jenis [2] = buffEE[6]; 
  harga_jenis [3] = buffEE[7]; 
  harga_jenis [4] = buffEE[8]; 
  harga_jenis [5] = buffEE[9];
  resetInput_init();
}
void load_nilaix()
{
   buffEE[1] = harga_layanan[1] ;
   buffEE[2] = harga_layanan[2] ;
   buffEE[3] = harga_layanan[3] ;
   buffEE[4] = harga_layanan[4] ;

   buffEE[5] = harga_jenis [1] ; 
   buffEE[6] = harga_jenis [2] ; 
   buffEE[7] = harga_jenis [3] ; 
   buffEE[8] = harga_jenis [4] ; 
   buffEE[9] = harga_jenis [5] ; 
}

void EEPROM_saveCek()
{
  for (int i = 0; i <= 11 ; i++) {
      if (buffEE[i] != harga[i]) {
        harga[i] = buffEE[i];
        memWriteInt(i* 10, buffEE[i]);
        Serial.print("Simpan ");
        Serial.print(i);
        Serial.print(" = ");
        Serial.println(harga[i]);
      }
    }
}
void init_EE()
{   
    Serial.println("EEPROM");
    if(memReadInt(0) !=EEPROM_CEK )
    {
       memWriteInt(0, EEPROM_CEK);
       memWriteInt(10, 3000);
       memWriteInt(20, 3000);
       memWriteInt(30, 3000);
       memWriteInt(40, 3000);
       memWriteInt(50, 3000);
       memWriteInt(60, 3000);
       memWriteInt(70, 3000);
       memWriteInt(80, 3000);
       memWriteInt(90,  0); 
       memWriteInt(100, 0);
       memWriteInt(110, 0);        
    }
    
      harga[1] = memReadInt(10);
      harga[2] = memReadInt(20);
      harga[3] = memReadInt(30);
      harga[4] = memReadInt(40);
      harga[5] = memReadInt(50);
      harga[6] = memReadInt(60);
      harga[7] = memReadInt(70);
      harga[8] = memReadInt(80); 
      harga[9] = memReadInt(90); 
      harga[10]= memReadInt(100); //estimasi
      harga[11]= memReadInt(110);//idunix

      buffEE[1] = harga[1];
      buffEE[2] = harga[2];
      buffEE[3] = harga[3];
      buffEE[4] = harga[4];
      buffEE[5] = harga[5];
      buffEE[6] = harga[6];
      buffEE[7] = harga[7];
      buffEE[8] = harga[8];
      buffEE[9] = harga[9];
      buffEE[10]= harga[10];
      buffEE[11]= harga[11];
      load_nilai();
}


void init_hx711()
{
  scale.set_scale();
  scale.tare();
  long zero_factor = scale.read_average();
  Serial.print("Zero factor: ");
  Serial.println(zero_factor);
}

void hx711_proses()
{
  scale.set_scale(calibration_factor);
  GRAM = scale.get_units();
  KG   = scale.get_units()/1000;
  

  if (KG<0){KG = 0;}
  
  Serial.print("Reading: ");
  Serial.print(GRAM);
  Serial.print(" Gram, ");
  Serial.print(KG);
  Serial.print(" KiloGram");
  Serial.print(" calibration_factor: ");
  Serial.print(calibration_factor);
  Serial.println();
  delay(100);

    if (Serial.available()) {
    char temp = Serial.read();
    if (temp == '+' || temp == 'a')
      calibration_factor += 0.1;
    else if (temp == '-' || temp == 'z')
      calibration_factor -= 0.1;
    else if (temp == 's')
      calibration_factor += 10;
    else if (temp == 'x')
      calibration_factor -= 10;
    else if (temp == 'd')
      calibration_factor += 100;
    else if (temp == 'c')
      calibration_factor -= 100;
    else if (temp == 'f')
      calibration_factor += 1000;
    else if (temp == 'v')
      calibration_factor -= 1000;
    else if (temp == 't')
      scale.tare();
  }
}

void RTC_now()
{
    DateTime now = rtc.now();
    Serial.print(now.year(), DEC);
    yearR = now.year(), DEC;
    Serial.print('/');
    Serial.print(now.month(), DEC);
    monthR = now.month(), DEC;
    Serial.print('/');
    Serial.print(now.day(), DEC);
    dayR = now.day(), DEC;
    Serial.print(" (");
    Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
    Serial.print(") ");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();

    Serial.print("Temperature: ");
    Serial.print(rtc.getTemperature());
    Serial.println(" C");
    epoch = now.unixtime();
    Serial.println( now.unixtime());
     

    
    DateTime future (now + TimeSpan(plus,0,0,0));
    Serial.print(future.year(), DEC);
    Fyear = future.year(), DEC;
    Serial.print('/');
    Serial.print(future.month(), DEC);
    Fmonth = future.month(), DEC;
    Serial.print('/');
    Serial.print(future.day(), DEC);
    Fday = future.day(), DEC;
    Serial.print(' ');
    Serial.print(future.hour(), DEC);
    Serial.print(':');
    Serial.print(future.minute(), DEC);
    Serial.print(':');
    Serial.print(future.second(), DEC);
    Serial.println();
   Serial.println("=======================================");
}

void printFrame()
{
  lcd.setCursor(1,0);
  lcd.print("------------------");
  lcd.setCursor(1,3);
  lcd.print("------------------");
  lcd.setCursor(0,1);
  lcd.write(byte(0));
  lcd.setCursor(0,2);
  lcd.write(byte(0));
  lcd.setCursor(19,1);
  lcd.write(byte(0));
  lcd.setCursor(19,2);
  lcd.write(byte(0));
  lcd.setCursor(0,0);
  lcd.write(byte(1));
  lcd.setCursor(19,0);
  lcd.write(byte(2));
  lcd.setCursor(0,3);
  lcd.write(byte(3));
  lcd.setCursor(19,3);
  lcd.write(byte(4));
}

void send_json(int id,int dayn,int dayf,String jns,int est,int brt, int hrg) {
  
  StaticJsonDocument<256> doc;
  doc["id"]    = id;   //id
  doc["dF"]    = dayf; //tanggal ambil
  doc["dN"]    = dayn; //tanggal masuk
  doc["jn"]    = jns;  //jenis barang
  //doc["pl"]    = plh;  //pelayanan
  doc["est"]   = est;  //estimasi hari
  doc["brt"]   = brt;  //berat
  doc["hrg"]   = hrg;  //harga
 

  serializeJson(doc, linkSerial);
  serializeJson(doc, Serial);
  Serial.println();
}
 
 
 void printer_out()
 { 
  //DateTime now = rtc.now();
  
  Serial.println("printing.....");
  lcd.clear();
  printFrame();
        lcd.setCursor(6, 1);
        lcd.print("Printing"); 
        
        while(1)
{
printer.setSize('L'); 
printer.boldOn(); 
printer.doubleHeightOn(); 
printer.justify('C'); 
printer.println("*SIJA LAUNDRY*");
printer.justify('R'); 
printer.boldOff(); 
printer.doubleHeightOff(); 
printer.println("    Jl. TMII pintu 2 atas      ");
printer.println("          Sawo No110           ");
printer.println("       082 114 295 038         ");
printer.println("*******************************");
printer.boldOn(); 
printer.setSize('L');
printer.justify('C');
printer.println(ID); 
printer.justify('R'); 
printer.boldOff(); 
printer.setSize('S');
printer.println("*******************************");
printer.justify('L');
printer.println(BuffMenu_input_jenis[pilihan [2]]);
printer.justify('R');
printer.print  ("Rp.");
printer.print  (harga_jenis[pilihan [2]]);
printer.println("/KG");
printer.justify('L');
printer.println(BuffMenu_input_layanan[pilihan [3]]);
printer.justify('R');
printer.print  ("Rp.");
printer.print  (harga_layanan [pilihan [3]]);
printer.println("/KG");
printer.println("-------------------------------");
printer.print  ("Quantity                ");
printer.print  (minimal);
printer.println("KG");
printer.println("-------------------------------");
printer.println("sub total                      ");
printer.print  ("RP.");
printer.println(hitung);
printer.println("-------------------------------");
printer.print  ("Tanggal masuk        ");
printer.justify('R'); 
printer.print  (dayR);
printer.print  ('/');
printer.print  (monthR);
printer.print  ('/');
printer.println(yearR);
printer.print("estimasi waktu           ");
printer.print( harga[10]);
printer.println(" hari");
printer.print  ("pengambilan          ");
printer.justify('R'); 
printer.print  (Fday);
printer.print  ('/');
printer.print  (Fmonth);
printer.print  ('/');
printer.println(Fyear);
printer.println("*******************************");
printer.println("Mohon disimpan sebagai bukti");
printer.println("transaksi yang sah");
printer.println("*******************************");
printer.boldOn();
printer.justify('C');
printer.setSize('L');
printer.println("Terima kasih");

printer.feed(); 
printer.feed(); 
printer.feed();
beep();

int DAYN = epoch; 
int DAYF = epoch+(harga[10]*86.400);
String p = BuffMenu_input_layanan[pilihan[3]];
String n = BuffMenu_input_jenis[pilihan[2]];
String JNS     = p+ "," + n;
int PLH     = pilihan [3];
int EST     = harga[10];
int BRT     = minimal;
int HRG     = hitung;
send_json(ID,DAYN,DAYF,JNS,EST,BRT,HRG); 
break;
}
    lcd.clear();
    printFrame();
 }

void initChar()
{
  lcd.createChar(7, mychar);
  lcd.createChar(0, verticalLine);
  lcd.createChar(1, char1);
  lcd.createChar(2, char2);
  lcd.createChar(3, char3);
  lcd.createChar(4, char4);
}

void printFrameUP()
{
  lcd.setCursor(1,0);
  lcd.print("------------------");
  lcd.setCursor(0,0);
  lcd.write(byte(1));
  lcd.setCursor(19,0);
  lcd.write(byte(2));
}

void loop_keypad()
{
   char tombol_push = keypad.getKey();
  
   if(tombol_push == '1'){loopKEYPADint = 1; }
   if(tombol_push == '2'){loopKEYPADint = 2; }
   if(tombol_push == '3'){loopKEYPADint = 3; }
   if(tombol_push == '4'){loopKEYPADint = 4; }
   if(tombol_push == '5'){loopKEYPADint = 5; }
   if(tombol_push == '6'){loopKEYPADint = 6; }
   if(tombol_push == '7'){loopKEYPADint = 7; }
   if(tombol_push == '8'){loopKEYPADint = 8; }
   if(tombol_push == '9'){loopKEYPADint = 9; }
   if(tombol_push == '0'){loopKEYPADint = 0; }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void IInput(int menu, int item)
{ resetInput();
  int flag_out = 0 ;
  menu_input = 0;
  lcd.clear();
  printFrame();
  lcd.setCursor(6, 0);
  lcd.print("Setting");
  delay(300);
  //printFrame();
  
          lcd.setCursor(1, 1);
          if (menu == input_pelayanan)    {lcd.print(BuffMenu_input_layanan[item]);}
     else if (menu == input_jenis_pakaian){lcd.print(BuffMenu_input_jenis[item]);}
     else if (menu == input_estimasi_Hari){lcd.print("Estimasi Hari ");lcd.print( harga[10]);}
           
  lcd.setCursor(1, 2);
  int buff;
  while(1)
  {
    char key = keypad.getKey();
 
  if (key) {
    switch (key)
    {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
 
        millisKeypad = millis() + periodaKeypad;
        if ((key == keySebelumnya) || (keySebelumnya == 0))
        {
          padChar = pad[key - '0'][charCounter];
          keySebelumnya = key;
        }
        else if ((padDitekan) && (padCounter < sizeof(bufferKeypad) - 1))
        {
          *bufferKeypadPtr++ = padChar;
          keySebelumnya = key;
          charCounter = 0;
          padCounter++;
          padChar = pad[key - '0'][charCounter];
        }
 
        padDitekan = true;
 
        lcd.setCursor(padCounter+1, 2);
        lcd.print(padChar);
        lcd.setCursor(padCounter+1, 2);
 
        charCounter++;
        if (!pad[key - '0'][charCounter])
        {
          charCounter = 0;
        }
        break;
      case 'D':
        lcd.clear();
        lcd.print("");
        delay(500);
        flag_out = 1;
        if (menu == input_estimasi_Hari){printFrame();lcd.setCursor(19,3);lcd.write(byte(0));}
        break;
      case 'C':
        if (padCounter)
        {
          if (keySebelumnya)
          {
            keySebelumnya = 0;
          }
          lcd.setCursor(padCounter+1, 2);
          lcd.print(' ');
          charCounter = 0;
          padCounter--;
          bufferKeypadPtr--;
          padChar = *bufferKeypadPtr;
 
          lcd.setCursor(padCounter+1, 2);
        }
        else
        {
          resetInput();
        }
        break;
      case '*':
        if ((padDitekan) && (padCounter < sizeof(bufferKeypad) - 1))
        {
          *bufferKeypadPtr++ = padChar;
        }
        *bufferKeypadPtr = 0;
 
        Serial.print("String input = ");
        Serial.println(bufferKeypad);
 
        lcd.clear();
        lcd.noBlink();
        lcd.clear();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Setting");
        delay(200);
        lcd.setCursor(6, 1);
        lcd.print("Saving");
        delay(500);
        lcd.setCursor(6, 2);
        lcd.print(bufferKeypad);
        String buffS = bufferKeypad;
        int buffI = buffS.toInt();
        Serial.print("BUFFER = ");
        Serial.println (buffI);
        Serial.print("menu = ");
        Serial.println(menu);
        Serial.print("Pilihan Item = ");
        Serial.println(item);
        
        if      (menu == input_pelayanan){harga_layanan[item] = buffI;}
        else if (menu == input_jenis_pakaian){harga_jenis[item] = buffI;}
        else if (menu == input_estimasi_Hari){ buffEE[10] = buffI; }
        delay(200);
        load_nilaix();
        delay(2700);
        EEPROM_saveCek();
        lcd.clear();
        resetInput();
        flag_out = 1;
        lcd.clear();
        lcd.noBlink();
        if (menu == input_estimasi_Hari)
        {printFrame();
          lcd.setCursor(19,3);
          lcd.write(byte(0));}
        
        break;
        

    }
  }
 
  if ((padDitekan) && (padCounter < sizeof(bufferKeypad) - 1))
  {
    if (millisKeypad < millis())
    {
      *bufferKeypadPtr++ = padChar;
      keySebelumnya = key;
      charCounter = 0;
      padCounter++;
      padDitekan = false;
 
      lcd.setCursor(padCounter+1, 2);
      lcd.print(' ');
      lcd.setCursor(padCounter+1, 2);
    }
  }
    if (flag_out == 1){break;}
  }
}


void pelayanan()
{      lcd.noBlink();
        lcd.clear();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Setting");
        delay(200);
        lcd.setCursor(3, 1);
        lcd.print("Harga Pelayanan");
        delay(500);
        lcd.clear();
        Serial.println("Harga Pelayanan");
        int keyy;
        //printFrameUP();
        while(1)
        { 
           pos_menu = input_pelayanan;
           lcd.setCursor(0, 0);
           lcd.print("1.Cuci");
           lcd.setCursor(0, 1);
           lcd.print("2.Gosok");
           lcd.setCursor(0, 2);
           lcd.print("3.Cuci&gosok");
           lcd.setCursor(0, 3);
           lcd.print("4.Cugostar");

           lcd.setCursor(13, 0);
           lcd.print("Rp");
           lcd.setCursor(13, 1);
           lcd.print("Rp");
           lcd.setCursor(13, 2);
           lcd.print("Rp");
           lcd.setCursor(13, 3);
           lcd.print("Rp");

           lcd.setCursor(15, 0);
           lcd.print(harga[1]);
           lcd.setCursor(15, 1);
           lcd.print(harga[2]);
           lcd.setCursor(15, 2);
           lcd.print(harga[3]);
           lcd.setCursor(15, 3);
           lcd.print(harga[4]);

          
        char tombol = keypad.getKey();
        if(tombol == 'D')
        {
        lcd.clear();
        lcd.print("");
        delay(500);
        printFrame();
        break;
        }
        if(tombol == '1' ){keyy = 1; IInput(pos_menu ,keyy);}
        if(tombol == '2' ){keyy = 2; IInput(pos_menu ,keyy);}
        if(tombol == '3' ){keyy = 3; IInput(pos_menu ,keyy);}
        if(tombol == '4' ){keyy = 4; IInput(pos_menu ,keyy);}
        //if(tombol == '5' ){keyy = 5; IInput(pos_menu ,keyy);}
        }
}
void jenis()
{
        lcd.clear();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Setting");
        delay(200);
        lcd.setCursor(3, 1);
        lcd.print("jenis pakaian");
        delay(500);
        lcd.clear();
        Serial.println("Jenis pakaian");
        
        //printFrameUP();
        int keyy;
        int menu_i = 0;
        while(1)
        { 
          switch(menu_i){
          case 0 :
           pos_menu = input_jenis_pakaian;
           lcd.setCursor(0, 0);
           lcd.print("1.Jas      ");
           lcd.setCursor(0, 1);
           lcd.print("2.Baju     ");
           lcd.setCursor(0, 2);
           lcd.print("3.Jeans    ");
           lcd.setCursor(0, 3);
           lcd.print("4.Selimut  ");

           lcd.setCursor(13, 0);
           lcd.print("Rp");
           lcd.setCursor(13, 1);
           lcd.print("Rp");
           lcd.setCursor(13, 2);
           lcd.print("Rp");
           lcd.setCursor(13, 3);
           lcd.print("Rp");

           lcd.setCursor(15, 0);
           lcd.print(harga[5]);
           lcd.setCursor(15, 1);
           lcd.print(harga[6]);
           lcd.setCursor(15, 2);
           lcd.print(harga[7]);
           lcd.setCursor(15, 3);
           lcd.print(harga[8]);
           break;
           
           case 1 :
           pos_menu = input_jenis_pakaian;
           lcd.setCursor(0, 0);
           lcd.print("5.Boneka");
           lcd.setCursor(0, 1);
           lcd.print("                    ");
           lcd.setCursor(0, 2);
           lcd.print("                    ");
           lcd.setCursor(0, 3);
           lcd.print("                    ");

           lcd.setCursor(13, 0);
           lcd.print("Rp");
//           lcd.setCursor(13, 1);
//           lcd.print("Rp");
//           lcd.setCursor(13, 2);
//           lcd.print("Rp");
//           lcd.setCursor(13, 3);
//           lcd.print("Rp");

           lcd.setCursor(15, 0);
           lcd.print(harga[9]);
//           lcd.setCursor(15, 1);
//           lcd.print(harga[6]);
//           lcd.setCursor(15, 2);
//           lcd.print(harga[7]);
//           lcd.setCursor(15, 3);
//           lcd.print(harga[8]);
           break;
          }
          
        char tombol = keypad.getKey();
        if(tombol == 'D')
        {
        lcd.clear();
        lcd.print("");
        delay(500);
        printFrame();
        break;
        }
        if(tombol == '1' ){keyy = 1; IInput(pos_menu ,keyy);}
        if(tombol == '2' ){keyy = 2; IInput(pos_menu ,keyy);}
        if(tombol == '3' ){keyy = 3; IInput(pos_menu ,keyy);}
        if(tombol == '4' ){keyy = 4; IInput(pos_menu ,keyy);}
        if(tombol == '5' ){keyy = 5; IInput(pos_menu ,keyy);}

        if(tombol == 'A' ){menu_i = 0;}
        if(tombol == 'B' ){menu_i = 1;}
//        if(tombol == '6' ){IInput();}
//        if(tombol == '7' ){IInput();}
//        if(tombol == '8' ){IInput();}
//        if(tombol == '9' ){IInput();}
//        if(tombol == '0' ){IInput();}
             
        }
}

void estimasi()
{
        lcd.clear();
        printFrame();
        lcd.setCursor(8, 0);
        lcd.print("MENU");
        delay(200);
        IInput(input_estimasi_Hari ,1);
}
void setting ()
{ lcd.noBlink();      
  lcd.clear();
        printFrame();
        lcd.setCursor(8, 0);
        lcd.print("MENU");
        delay(200);
        lcd.setCursor(3, 1);
        lcd.print("Setting harga");
        Serial.println("Setting");
        delay(1000);
        lcd.clear();
        printFrame();
          lcd.setCursor(19,3);
          lcd.write(byte(0));
          char tombol = keypad.getKey();
        while(1)
        {
        tombol = keypad.getKey();
        lcd.setCursor(6, 0);
        lcd.print("Setting");
        lcd.setCursor(0, 1);
        lcd.write(7);
        lcd.print("A Pelayanan");
        lcd.setCursor(0, 2);
        lcd.write(7);
        lcd.print("B Jenis Pakaian");
        lcd.setCursor(0, 3);
        lcd.write(7);
        lcd.print("C Estimasi Hari   ");

       
      if(tombol == 'D')
        {
        lcd.clear();
        printFrame();
        lcd.setCursor(8, 0);
        lcd.print("MENU");
        delay(200);
        lcd.setCursor(5, 1);
        lcd.print("HOMESCREAN");
        delay(500);
        break;
        }
      if(tombol == 'A'){pelayanan();}
      if(tombol == 'B'){jenis();}
      if(tombol == 'C'){estimasi();}
        }      
}


void timbang()
{       lcd.noBlink();
        Serial.println("timbang");
        lcd.clear();
        load_nilai();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Timbang");
        delay(200);
        lcd.setCursor(3, 1);
        lcd.print("Timbang Berat");
        delay(1000);
        lcd.clear();
        printFrame();
        menu_proses_timbang = 0;
        int p = 0;
        if (ID == 30000){ID=0;}
        ID = buffEE[11];
        ID = ID+1;
        
    while(1)
        { 
          char tombol = keypad.getKey();
          Serial.print("Berat = ");
          Serial.print(hasil[1]);
          Serial.print(", Jenis baju = ");
          Serial.print(hasil[2]);
          Serial.print(", Pelayanan = ");
          Serial.print(hasil[3]);
          Serial.print("  ,");
          Serial.println(menu_proses_timbang);
          
      
  if(tombol == 'D')
  {
        lcd.clear();
        printFrame();
        lcd.setCursor(8, 0);
        lcd.print("MENU");
        delay(200);
        lcd.setCursor(5, 1);
        lcd.print("HOMESCREAN");
        delay(500);
        break;
  }
  else if(tombol == '#')
  {     
        lcd.clear();
        printFrame();
        delay(200);
        lcd.setCursor(6, 1);
        lcd.print("SET TARE");
        scale.tare();
        delay(1000);
        lcd.clear();
        printFrame();
  }
   else if(tombol == 'A')
      {
      menu_proses_timbang = menu_proses_timbang-1; 
      if ( menu_proses_timbang > -1 )
        {
        lcd.clear();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Timbang");
        delay(200);
        lcd.setCursor(6, 2);
        lcd.print("Kembali");
        delay(1000);
        lcd.clear();
        
        }else
        {menu_proses_timbang = menu_proses_timbang+1;}   
      }
    else if(tombol == 'C')
        {menu_proses_timbang = 0;}
    else if(tombol == 'B')
  {     if (menu_proses_timbang == menu_preview )
            {
              buffEE[11]=ID;
              EEPROM_saveCek();
              printer_out();
              
              break;
            }

        menu_proses_timbang = menu_proses_timbang+1;
        if ( menu_proses_timbang <2)
        {
        hasil[1] = KG;
        Serial.println(hasil[1]);
        lcd.clear();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Timbang");
        delay(200);
        lcd.setCursor(7, 2);
        lcd.print("Simpan");
        delay(1000);
        lcd.clear();
        }else
        {menu_proses_timbang = menu_proses_timbang-1;}       
  }


   if(tombol == '1' || tombol == '2' || tombol == '3' 
   || tombol == '4' || tombol == '5' || tombol == '6' 
   || tombol == '7' || tombol == '8' || tombol == '9' 
   || tombol == '0' )
   {
        if(tombol == '1'){loopKEYPADint = 1; }
   else if(tombol == '2'){loopKEYPADint = 2; }
   else if(tombol == '3'){loopKEYPADint = 3; }
   else if(tombol == '4'){loopKEYPADint = 4; }
   else if(tombol == '5'){loopKEYPADint = 5; }
   else if(tombol == '6'){loopKEYPADint = 6; }
   else if(tombol == '7'){loopKEYPADint = 7; }
   else if(tombol == '8'){loopKEYPADint = 8; }
   else if(tombol == '9'){loopKEYPADint = 9; }
   else if(tombol == '0'){loopKEYPADint = 0; } 
   
       if(menu_proses_timbang == menu_jenis)
       {
        hasil[2] = harga_jenis[loopKEYPADint];
        menu_proses_timbang = menu_proses_timbang+1;
        pilihan [2]=loopKEYPADint;
        lcd.clear();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Timbang");
        delay(200);
        lcd.setCursor(7, 1);
        lcd.print("Simpan");
        delay(1000);
        lcd.clear();
       }
       else if(menu_proses_timbang == menu_pelayanan)
       {
        hasil[3] = harga_layanan[loopKEYPADint];
        menu_proses_timbang =  menu_proses_timbang+1;
        pilihan [3]=loopKEYPADint;
        lcd.clear();
        printFrame();
        lcd.setCursor(6, 0);
        lcd.print("Timbang");
        delay(200);
        lcd.setCursor(7, 1);
        lcd.print("Simpan");
        delay(1000);
        lcd.clear();
       }
   }
   switch (menu_proses_timbang) {
      case 0:
          tombol = keypad.getKey();
          printFrame();
          hx711_proses();
          lcd.setCursor(3, 1);
          lcd.print("Berat Pakaian");
          
          lcd.setCursor(7, 2);
          if (GRAM < min_GRAM){
          lcd.print("ERROR   ");}
          else
          {
          lcd.print("     ");
          lcd.setCursor(7, 2);
          lcd.print(KG);
          lcd. print (" KG");}
          
        break;
    
      case 1:
           tombol = keypad.getKey();
           lcd.setCursor(0, 0);
           lcd.print("1.Jas");
           lcd.setCursor(0, 1);
           lcd.print("2.Baju");
           lcd.setCursor(0, 2);
           lcd.print("3.Jeans");
           lcd.setCursor(0, 3);
           lcd.print("4.Selimut");

           lcd.setCursor(11, 0);
           lcd.print("5.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("6.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("7.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("8.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("9.Boneka");
          loop_keypad();
          
        break;
         case 2:
           tombol = keypad.getKey();
           lcd.setCursor(0, 0);
           lcd.print("1.Cuci");
           lcd.setCursor(0, 1);
           lcd.print("2.Gosok");
           lcd.setCursor(0, 2);
           lcd.print("3.Cuci&gosok");
           lcd.setCursor(0, 3);
           lcd.print("4.Cugostar");

           lcd.setCursor(11, 0);
          // lcd.print("5.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("6.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("7.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("8.Boneka");
           lcd.setCursor(11, 0);
          // lcd.print("9.Boneka");
           p= 0;
        break;

       case 3:
           tombol = keypad.getKey();
           if (p == 0){p++;printFrame();}
           RTC_now();
           lcd.setCursor(0, 0);
           lcd.print("ID = ");
           lcd.print(ID);
           lcd.print(' ');
           lcd.setCursor(1, 3);
           lcd.print(minimal);
           lcd.print("KG");
           lcd.setCursor(10, 3);
           lcd.print(dayR);
           lcd.print('/');
           lcd.print(monthR);
           lcd.print('/');
           lcd.print(yearR);
         
           lcd.setCursor(2, 1);
           lcd.print(BuffMenu_input_jenis[pilihan [2]]);
           lcd.print(",");
           lcd.print(BuffMenu_input_layanan[pilihan [3]]);
           lcd.setCursor(1, 2);
           lcd.print("    Rp. ");
           lcd.print(hitung);
           lcd.print(" ");

                
            if (hasil[1] <= 1){minimal = 1;}
            else 
            {
           
           minimal = round(hasil[1]);} 
           
           float a = hasil[1] - minimal;
           if (a<0.00)
           {Serial.print(", ");}else{Serial.print("rule 1 , ");minimal = minimal + 1; }
           Serial.println(a);
           
           
           int jumlahT = hasil[2] + hasil[3];
           hitung = minimal*jumlahT;
           Serial.print(" Berat = ");
           Serial.print(minimal);
           Serial.print(" , ");
           Serial.print(hasil[2]);
           Serial.print("+");
           Serial.print(hasil[3]);
           Serial.print("");
           Serial.print(hasil[2]+hasil[3]);
           Serial.print(" , hasil = ");
           Serial.println(hitung);
            plus = harga[10];
           break;
           
           
         
    }
 delay(200);

}
} 

void displayHomeScreen() {

  if ((millis() - lastTime) > timerDelay) {
        RTC_now();
        Serial.println("home screan");
        lcd.setCursor(8, 0);
        lcd.print("MENU");
        lcd.setCursor(0, 1);
        lcd.write(7);
        lcd.print("A Input/ubah harga");
        lcd.setCursor(0, 2);
        lcd.write(7);
        lcd.print("B Timbang");    
   
    lastTime = millis();
  }}
 
void setup() {
  
  Wire.begin();
  Serial.begin(9600);
  Serial3.begin(9600);
  linkSerial.begin(9600);
  pinMode(buzzer, OUTPUT);
  printer.begin();
  lcd.init();
  lcd.init();
  lcd.setCursor(1,2);
  lcd.blink();
  initChar();
  printFrame();
  initRTC();
  init_EE();
  init_hx711();
  beep();
  lcd.noBlink();
  lcd.backlight();
}

void loop() {
   
displayHomeScreen();
tombolbuff = keypad.getKey();
  if (tombolbuff == 'A') {
        setting();
  }
    if (tombolbuff == 'B') {
        timbang();
  }
  
}
